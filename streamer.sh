# Default-Start:  2 3 4 5
# Default-Stop:   0 1 6
# Short-Description:    
# Description:    
### END INIT INFO

#########################################################################
################################Settings#################################
#########################################################################
#####################You may change these Settings#######################
#########################################################################

########The username to execute this script needs sudo rights############
USERNAME=`whoami`

########The directory is absolute, do not use relative paths#############
DIRECTORY=/home/$USERNAME
HOSTNAME=bolderbay.net
INVOCATION="cvlc -I dummy "


ME=`whoami`
as_user() {
  if [ $ME == $USERNAME ] ; then
    bash -c "$1"
  else
    su - $USERNAME -c "$1"
  fi
}

stream_start_conf () {

if [ "$1" -a "$2" -a "$3" ]
then
  Input="$DIRECTORY/playlists/$1"
  Name="$2"
  Port="$3"
  if [ "$4" -a "$5" ]
  then
    VCodec="$4"
    ACodec="$5"
    STREAM="$Input --loop --sout-all --sout-keep --sout-transcode-audio-sync --sout-avcodec-strict=-2 --sout '#transcode{vcodec=$VCodec,acodec=$ACodec,ab=128,channels=2,samplerate=44100}:gather:rtp{mux=ts,sdp=rtsp://$HOSTNAME:$Port/$Name.sdp}' "
  else
    STREAM="$Input --loop --sout-all --sout-keep --sout-transcode-audio-sync --sout-avcodec-strict=-2 --sout '#gather:rtp{mux=ts,sdp=rtsp://$HOSTNAME:$Port/$Name.sdp}' "
  fi
  
  as_user "screen -dmS $Name $INVOCATION $STREAM"
  sleep 3
  if screen -list | grep -q $Name
  then
    echo "Stream started!"
  else
    echo "Could not start Stream"
  fi
else
  echo "Not enough arguments"
fi

}

stream_start () {

if [ "$1" -a "$2" -a "$3" ]
then
  Input="$DIRECTORY/media/$1"
  Name="$2"
  Port="$3"
  if [ "$4" -a "$5" ]
  then
    VCodec="$4"
    ACodec="$5"
    STREAM="$Input --loop --sout-keep --sout-transcode-audio-sync --sout-avcodec-strict=-2 --sout '#transcode{vcodec=$VCodec,acodec=$ACodec,ab=128,channels=2,samplerate=44100}:gather:rtp{mux=ts,sdp=rtsp://$HOSTNAME:$Port/$Name.sdp}' "
  else
    if [ "$4" == "nomux" ]
	then
	  STREAM="$Input --loop --sout-keep --sout-transcode-audio-sync --sout-avcodec-strict=-2 --sout '#gather:rtp{sdp=rtsp://$HOSTNAME:$Port/$Name.sdp}' "	  
	else
	  STREAM="$Input --loop --sout-keep --sout-transcode-audio-sync --sout-avcodec-strict=-2 --sout '#gather:rtp{mux=ts,sdp=rtsp://$HOSTNAME:$Port/$Name.sdp}' "
	fi
  fi
  as_user "screen -dmS $Name $INVOCATION $STREAM"
  sleep 3
  if screen -list | grep -q $Name
  then
    echo "Stream started!"
  else
    echo "Could not start Stream"
  fi
else
  echo "Not enough arguments"
fi

}

stream_stop () {
	
if [ "$1" ]
then
  Name="$1"
  if screen -list | grep -q $Name
  then
    kill $(ps au | grep $Name | grep vlc | awk '{print $2}')
	sleep 5
    if screen -list | grep -q $Name
    then
      echo "Stream could not be stopped"
    else
      echo "Stream stopped"
    fi
  else
    echo "Stream was not running"
  fi
else
  echo "Not enough arguments"
fi

}


#Start-Stop here
case "$1" in
  start)
    stream_start "$2" "$3" "$4" "$5" "$6"
	;;
  startconf)
    stream_start_conf "$2" "$3" "$4" "$5" "$6"
	;;
  stop)
    stream_stop "$2"
    ;;
  *)
  echo "Usage:"
  echo "e.g.  ./streamer.sh command"
  echo "###########################################################################################################"
  echo "Commands are:"
  echo "start      Input Name Port   Optional:(Videocodec Audiocodec)  | Starts a stream from Input on Port with given Name "
  echo "startconf  Input Name Port   Optional:(Videocodec Audiocodec)  | Starts a stream from Input Playlist on Port with given Name "
  echo "stop             Name                                          | Stops stream with Name"
  echo "###########################################################################################################"
  echo "If no video and audio codec is supplied no transcoding of output occurs"
  echo "###########################################################################################################"
  echo "VideoCodecs: h264, mp4v, h265"
  echo "AudioCodecs: a52, mp4a, mp3"
  echo "###########################################################################################################"
  echo "Created by Vincent Sylvester of Zetamex"
  echo "DO NOT USE FOR COMMERCIAL APPLICATIONS"
  echo "###########################################################################################################"
  exit 1
  ;;
esac

exit 0
