CVLC RTSP Streaminging management script



Requires a complete install of vlc and cvlc along with any codecs used in media and output.

Create a media folder for all the media to go in and a playlists folder for the playlists.



Usage example:

./streamer.sh startconf docu.xspf docu 38000 mp4v a52



starts an rtsp stream with docu.xspf as input on port 38000 with videocodec mp4v and audiocodec a52

h264 and mp4a seem to be the most stable for rtsp output

If no audio and video codec is given no transcoding of output occurs.

Standard .xspf playlists can be used, tracklisting should consist of solely:

<track>
	<location>file:///%%FILELOCATION%%</location>
	<duration>21033</duration>
	<extension application="http://www.videolan.org/vlc/playlist/0">
		<vlc:id>%%FILE_ID%%</vlc:id>
	</extension>
</track>

Remove any titles or artist additions.

Does not support mkv or multistream containers, best use mp4 h264 encoded files.



##Remember to change the hostname variable inside the script##